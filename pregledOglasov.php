<?php

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>kolo.si</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/fonts/line-icons.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slicknav.css">
    <link rel="stylesheet" type="text/css" href="assets/css/color-switcher.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
</head>

<body>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-7 col-xs-12">
                    <div class="header-top-right float-right">
                        <?php
                            if ($logged == "1") {
                        ?>
                        <a href="<?php echo $myrole ; ?>" class="header-top-button">Moj nalog</a> |
                        <a href="logout" class="header-top-button">Odjava</a>
                        <?php
                            }
                            else {
                        ?>
                        <a href="login" class="header-top-button">Prijava</a> |
                        <a href="register" class="header-top-button">Register</a>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg bg-white fixed-top scrolling-navbar">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"
                    aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="lni-menu"></span>
                    <span class="lni-menu"></span>
                    <span class="lni-menu"></span>
                </button>
                <a id="site_logo" class="navbar-brand">kolo.si</a>
            </div>
            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="navbar-nav mr-auto w-100 justify-content-center">
                    <li class="nav-item active"><a class="nav-link" href="./">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="fav">Favorites</a></li>
                    <li class="nav-item"><a class="nav-link" href="place">Place ad</a></li>
                    <li class="nav-item"><a class="nav-link" href="req">My requests</a></li>
                    <li class="nav-item"><a class="nav-link" href="mess">Messages</a></li>
                    <li class="nav-item"><a class="nav-link" href="ads">My ads</a></li>
                </ul>
            </div>
        </div>
        <ul class="mobile-menu">
            <li><a class="active" href="./">Home</a></li>
            <li><a href="fav">Favorites</a></li>
            <li><a href="place">Place ad</a></li>
            <li><a href="req">My requests</a></li>
            <li><a href="mess">Messages</a></li>
            <li><a href="ads">My ads</a></li>
        </ul>
    </nav>

    <div id="hero-area">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-9 col-xs-12 text-center">
                    <div class="contents">
                        <h1 class="head-title">Iščete kolesa?</h1>
                        <p>Tukaj lahko najdete vse vrste kolesa.</p>
                        <div class="search-bar">
                            <div class="search-inner">
                                <form class="search-form" action="listings" autocomplete="off">
                                    <div class="form-group">
                                        <input type="text" name="keyword" class="form-control" placeholder="Iščete kolesa?">
                                    </div>
                                    <div class="form-group inputwithicon">
                                        <div class="select">
                                            <select name="mesto" required>
                                                <option value="all">Vsa mesta</option>
                                                <?php
                                                    try {
                                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                        $stmt = $conn->prepare("SELECT * FROM tbl_mesto ORDER BY mesto");
                                                        $stmt->execute();
                                                        $result = $stmt->fetchAll();
                                                        foreach($result as $row){
                                                            print '<option value="'.$row['mesto'].'">'.$row['mesto'].'</option>';
                                                        }              
                                                    }catch(PDOException $e){
                                                        echo "Connection failed: " . $e->getMessage();
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <i class="lni-target"></i>
                                    </div>
                                    <div class="form-group inputwithicon">
                                        <div class="select">
                                            <select name="vrsta" required>
                                                <option value="all">Vse vrste</option>
                                                <?php
                                                    try {
                                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                        $stmt = $conn->prepare("SELECT * FROM tbl_vrste ORDER BY vrsta");
                                                        $stmt->execute();
                                                        $result = $stmt->fetchAll();
                                                        foreach($result as $row){
                                                            print '<option value="'.$row['vrsta'].'">'.$row['vrsta'].'</option>';
                                                        }              
                                                    }catch(PDOException $e){
                                                        echo "Connection failed: " . $e->getMessage();
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <i class="lni-menu"></i>
                                    </div>
                                    <button type="submit" name="search" class="btn btn-common">Search</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery-min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/jquery.slicknav.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/form-validator.min.js"></script>
    <script src="assets/js/contact-form-script.min.js"></script>
    <script src="assets/js/summernote.js"></script>
</body>
</html>